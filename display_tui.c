/*
    Copyright 2013 Marek "p2004a" Rusinowski
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include "display.h"

#define CORNER_RIGHT_UP "\u2510"
#define CORNER_RIGHT_DOWN "\u2518"
#define CORNER_LEFT_UP "\u250c"
#define CORNER_LEFT_DOWN "\u2514"
#define CORNER_CENTER "\u253c"
#define HORIZONTAL_LINE "\u2500"
#define VERTICAL_LINE "\u2502"
#define HORIZONTAL_CORNER_DOWN "\u2534"
#define HORIZONTAL_CORNER_UP "\u252c"
#define VERTICAL_CORNER_LEFT "\u251c"
#define VERTICAL_CORNER_RIGHT "\u2524"
#define INDENT "        "
#define NEWLINE "\n"
#define CROSS "X"
#define CIRCLE "O"
#define EMPTY " "

#define FOREGROUND_DARK_GREY "\x1b[38;5;240m"
#define FOREGROUND_GREEN "\x1b[32m"
#define FOREGROUND_BOLD "\x1b[1m"
#define RESET_COLOR "\x1b[0m"

static struct {BoardFieldType type; int marked;} board[BOARD_SIZE][BOARD_SIZE];

void board_redraw() {
    int x, y;
    printf(NEWLINE);
    printf(INDENT);
    printf(FOREGROUND_DARK_GREY);
    printf(CORNER_LEFT_UP);
    for (x = 0; x < BOARD_SIZE; ++x) {
        printf(HORIZONTAL_LINE);
        if (x < BOARD_SIZE - 1) {
            printf(HORIZONTAL_CORNER_UP);
        }
    }
    printf(CORNER_RIGHT_UP);
    printf(RESET_COLOR);
    printf(NEWLINE);

    for (y = 0; y < BOARD_SIZE; ++y) {
        printf(INDENT);
        printf(FOREGROUND_DARK_GREY);
        printf(VERTICAL_LINE);
        printf(RESET_COLOR);
        for (x = 0; x < BOARD_SIZE; ++x) {
            if (board[x][y].marked) {
                printf(FOREGROUND_BOLD);
            }
            switch (board[x][y].type) {
                case empty: printf(EMPTY); break;
                case X: printf(CROSS); break;
                case O: printf(CIRCLE); break;
            }
            printf(RESET_COLOR);
            printf(FOREGROUND_DARK_GREY);
            printf(VERTICAL_LINE);
            printf(RESET_COLOR);
        }
        printf(NEWLINE);
        if (y < BOARD_SIZE - 1) {
            printf(INDENT);
            printf(FOREGROUND_DARK_GREY);
            printf(VERTICAL_CORNER_LEFT);
            for (x = 0; x < BOARD_SIZE; ++x) {
                printf(HORIZONTAL_LINE);
                if (x < BOARD_SIZE - 1) {
                    printf(CORNER_CENTER);
                }
            }
            printf(VERTICAL_CORNER_RIGHT);
            printf(RESET_COLOR);
            printf(NEWLINE);
        }
    }

    printf(INDENT);
    printf(FOREGROUND_DARK_GREY);
    printf(CORNER_LEFT_DOWN);
    for (x = 0; x < BOARD_SIZE; ++x) {
        printf(HORIZONTAL_LINE);
        if (x < BOARD_SIZE - 1) {
            printf(HORIZONTAL_CORNER_DOWN);
        }
    }
    printf(CORNER_RIGHT_DOWN);
    printf(RESET_COLOR);
    printf(NEWLINE);
}

void board_init() {
    int x, y;
    for (x = 0; x < BOARD_SIZE; ++x) {
        for (y = 0; y < BOARD_SIZE; ++y) {
            board[x][y].marked = 0;
            board[x][y].type = empty;
        }
    }
}

void board_free() {}

void board_place(BoardPos pos, BoardFieldType t) {
    board[pos.x][pos.y].type = t;
}

void board_mark(BoardPos pos) {
    board[pos.x][pos.y].marked = 1;
}

void board_unmark(BoardPos pos) {
    board[pos.x][pos.y].marked = 0;
}

BoardPos board_get_move() {
    BoardPos pos = {.x = -1, .y = -1};
    int incorrect = 0;
    while ( ! (pos.x <= BOARD_SIZE && pos.x >= 1 && pos.y <= BOARD_SIZE && pos.y >= 1)) {
        if (incorrect) printf("Illegal move\n");
        printf(FOREGROUND_GREEN);
        printf(FOREGROUND_BOLD);
        printf("> ");
        printf(RESET_COLOR);
        int res = scanf("%d%d", &pos.x, &pos.y);
        if (res != 2) {
            pos.x = -1;
            pos.y = -1;
            return pos;
        }
        incorrect = 1;
    }
    --pos.x; --pos.y;
    return pos;
}

void board_message(const char *format, ...) {
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
    printf("\n");
}
