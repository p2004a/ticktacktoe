/*
    Copyright 2013 Marek "p2004a" Rusinowski
*/
#include <stdio.h>
#include <string.h>

#include "display.h"
#include "ai.h"

inline int dleft_idx(int row, int col) {
    return col + row;
}

inline int dright_idx(int row, int col) {
    return (col - row) + BOARD_SIZE;
}

static int board[BOARD_SIZE][BOARD_SIZE];

inline BoardPos make_board_pos(int x, int y) {
    BoardPos pos = {.x=x, .y=y};
    return pos;
}

inline void mark_fields(BoardPos pos, BoardPos move) {
    while (pos.x >= 0 && pos.x < BOARD_SIZE && pos.y >= 0 && pos.y < BOARD_SIZE) {
        board_mark(pos);
        pos.x += move.x;
        pos.y += move.y;
    }
}

inline int mark_if_filled(int *val, BoardPos pos, BoardPos move, int player) {
    *val += player & 1 ? 1 : 1000;
    if (*val == BOARD_SIZE || *val == BOARD_SIZE * 1000) {
        mark_fields(pos, move);
        return 1;
    }
    return 0;
}

int mark_if_end_move(BoardPos move, int player) {
    static int vrow[BOARD_SIZE], vcol[BOARD_SIZE], vdleft[BOARD_SIZE * 2], vdright[BOARD_SIZE * 2];
    int res = 0;
    res += mark_if_filled(&vrow[move.y], make_board_pos(0, move.y), make_board_pos(+1, 0), player);
    res += mark_if_filled(&vcol[move.x], make_board_pos(move.x, 0), make_board_pos(0, +1), player);
    res += mark_if_filled(&vdleft[dleft_idx(move.y, move.x)], make_board_pos(BOARD_SIZE - 1, 0), make_board_pos(-1, +1), player);
    res += mark_if_filled(&vdright[dright_idx(move.y, move.x)], make_board_pos(0, 0), make_board_pos(+1, +1), player);
    return res;
}

int main(int argc, char *argv[]) {
    setvbuf(stdout, NULL, _IONBF, 0);

    int use_ai = 0;
    {
        int i;
        for (i = 0; i < argc; ++i) {
            if (strcmp(argv[i], "--ai") == 0) {
                use_ai = 1;
            }
            if (strcmp(argv[i], "--help") == 0) {
                fprintf(stderr, "USAGE:\n\t%s [options]\n\n\t--ai    play with computer\n\t--help  print this message\n\nCopyright 2013 Marek \"p2004a\" Rusinowski\n", argv[0]);
                return 0;
            }
        }
    }

    if (use_ai) ai_init();
    board_init();

    int player = 1;
    int finished = 0;

    int fields_left = BOARD_SIZE * BOARD_SIZE;

    while (1) {
        board_redraw();

        BoardPos move;
        if (player == 2 && use_ai) {
            ai_get_place(&move.x, &move.y);
        } else {
            move = board_get_move();
        }

        if (move.x == -1 && move.y == -1) {
            break;
        }

        if (finished) continue;

        if (board[move.x][move.y] != 0) {
                board_message("You can't place mark on this field");
                continue;
        }

        board[move.x][move.y] = player;

        board_place(move, player == 1 ? O : X);

        if (use_ai) ai_place(move.x, move.y, player);

        --fields_left;
        if (mark_if_end_move(move, player)) {
            finished = 1;
            board_message("Player %d won", player);
        } else if (fields_left == 0) {
            finished = 1;
            board_message("Dead Heat");
        }

        player ^= 3;
    }

    board_free();
    if (use_ai) ai_free();

    return 0;
}
