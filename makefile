GCCOPTS = -Wall -Wextra -O2 -fno-common
ALLEGRO_LIBS = -lallegro -lallegro_primitives -lallegro_dialog

prog: main.o display_allegro.o ai.o
	gcc main.o display_allegro.o ai.o -o prog $(ALLEGRO_LIBS) $(GCCOPTS)

prog_tui: main.o display_tui.o ai.o
	gcc main.o display_tui.o ai.o -o prog_tui $(GCCOPTS)

main.o: main.c display.h ai.h
	gcc -c main.c -o main.o $(GCCOPTS)

display_allegro.o: display.h display_allegro.c
	gcc -c display_allegro.c -o display_allegro.o $(GCCOPTS)

display_tui.o: display.h display_tui.c
	gcc -c display_tui.c -o display_tui.o $(GCCOPTS) --std=gnu99

ai.o: ai.c ai.h
	gcc -c ai.c -o ai.o $(GCCOPTS) -O3

.PHONY: clean
clean:
	rm *.o prog prog_tui
