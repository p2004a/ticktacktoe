/*
    Copyright 2013 Marek "p2004a" Rusinowski
*/
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <assert.h>

#include "ai.h"

typedef uint32_t State;

inline State state_set(State state, int x, int y, int val) {
    val &= 3;
    register int i = (y * 3 + x) * 2;
    state &= ~(0x03ULL << i);
    state |= (((State) val) << i);
    return state;
}

inline State state_set_idx(State state, int idx, int val) {
    val &= 3;
    idx *= 2;
    state &= ~(0x03ULL << idx);
    state |= (((State) val) << idx);
    return state;
}

inline int state_get(State state, int x, int y) {
    return (state >> ((y * 3 + x) * 2)) & 0x03;
}

inline int state_get_idx(State state, int idx) {
    return (state >> (idx * 2)) & 0x03;
}

static State global_state;
static int num_moves, *state_value; 

#define NONE 123653 /* magic number */

static int get_game_end(State state) {
    char y[] = {0,1,2,0,1,2,0,1,2,0,0,0,1,1,1,2,2,2,0,1,2,0,1,2};
    char x[] = {0,0,0,1,1,1,2,2,2,0,1,2,0,1,2,0,1,2,0,1,2,2,1,0};
    int p, b, i, j, set_fields = 0;

    for (i = 0; i < 3*8; i += 3) {
        p = state_get(state, x[i], y[i]);
        for (j = 0; j < 3; ++j) {
            b = state_get(state, x[i + j], y[i + j]);
            if (b) ++set_fields;
            if (b != p) p = 0;
        }
        switch (p) {
            case 1: return 1;
            case 2: return -1;
        }
    }
    return set_fields == 3 * 8 ? 0 : NONE;
}

static int minmax(State state, int player) {
    if (state_value[state] != NONE) {
        return state_value[state];
    }

    int val = get_game_end(state);
    if (val == NONE) {
        val = player == 1 ? -1000 : 1000;
        int i;
        for (i = 0; i < 3 * 3; ++i) {
            if (state_get_idx(state, i) == 0) {
                int res = minmax(state_set_idx(state, i, player), player ^ 3);
                switch (player) {
                    case 1: val = res > val ? res : val; break;
                    case 2: val = res < val ? res : val; break;
                }
            }
        }
    }
    state_value[state] = val;
    return val;
}

void ai_init() {
    state_value = (int *) calloc(300000, sizeof(int)); /* its around 2^18 */
    int i;
    for (i = 0; i < 300000; ++i) state_value[i] = NONE;
    global_state = 0;
    minmax(0, 1);
    num_moves = 0;
}

void ai_free() {
    free(state_value);
}

void ai_place(int x, int y, int player) {
    assert(x >= 0 && x < 3 && y >= 0 && y < 3 && player >= 1 && player <= 2);
    global_state = state_set(global_state, x, y, player);
    ++num_moves;
}

static int state_min(State a, State b) {
    return state_value[a] < state_value[b] ? 1 : 0;
}

static int state_max(State a, State b) {
    return state_value[a] > state_value[b] ? 1 : 0;
}

void ai_get_place(int *x, int *y) {
    int player = num_moves & 1 ? 2 : 1;
    int (*cmp)(State, State) = player == 2 ? state_max : state_min;

    *x = -1;
    *y = -1;

    if (get_game_end(global_state) != NONE) {
        return;
    }

    int i, j;
    for (j = 0; j < 3; ++j) {
        for (i = 0; i < 3; ++i) {
            if (state_get(global_state, i, j) == 0) {
                if (*x == -1 || (cmp)(state_set(global_state, *x, *y, player), state_set(global_state, i, j, player))) {
                    *x = i;
                    *y = j;
                }
            }
        }
    }
}
