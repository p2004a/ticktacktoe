/*
    Copyright 2013 Marek "p2004a" Rusinowski
*/
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

void ai_init();
void ai_free();
void ai_place(int x, int y, int player); /* player can be 1 or 2 */
void ai_get_place(int *x, int *y);

#ifdef __cplusplus
}
#endif
