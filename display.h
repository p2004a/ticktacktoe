/*
    Copyright 2013 Marek "p2004a" Rusinowski
*/
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define BOARD_SIZE 3

typedef enum {empty = 0, X, O} BoardFieldType;
typedef struct {int x; int y;} BoardPos;

void board_init();
void board_free();
void board_place(BoardPos pos, BoardFieldType t);
void board_mark(BoardPos pos);
void board_unmark(BoardPos pos);
BoardPos board_get_move();
void board_message(const char *format, ...);
void board_redraw();

#ifdef __cplusplus
}
#endif
