/*
    Copyright 2013 Marek "p2004a" Rusinowski
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <unistd.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_native_dialog.h>

#include "display.h"

#define BOARD_USER_EVENT_REDRAW 1000
#define BOARD_USER_EVENT_STOP 1001
#define BOARD_USER_EVENT_MESSAGE_BOX 1002

/* Beacuse we need only one board, and because program is multithreaded, for simplicity
we use global variables and allegro events for syncing data between threads etc */

static ALLEGRO_THREAD *board_thread = NULL;
static ALLEGRO_EVENT_SOURCE board_user_event_source;

static struct {BoardFieldType type; int marked;} board[BOARD_SIZE][BOARD_SIZE];
static ALLEGRO_MUTEX *board_mutex;

#define MOVES_SIZE 100
static BoardPos moves[MOVES_SIZE];
static volatile int moves_begin = 0, moves_end = 0;
static ALLEGRO_MUTEX *moves_mutex;
static ALLEGRO_COND *moves_cond;

static void push_move(BoardPos move) {
    al_lock_mutex(moves_mutex);
    moves[moves_end] = move;
    moves_end = (moves_end + 1) % MOVES_SIZE;
    al_broadcast_cond(moves_cond);
    al_unlock_mutex(moves_mutex);
}

static BoardPos pop_move() {
    BoardPos res;
    al_lock_mutex(moves_mutex);
    while (moves_end == moves_begin) {
        al_wait_cond(moves_cond, moves_mutex);
    }
    res = moves[moves_begin];
    moves_begin = (moves_begin + 1) % MOVES_SIZE;
    al_unlock_mutex(moves_mutex);
    return res;
}

#define NUM_BITMAPS 3
#define X_BITMAP 0
#define O_BITMAP 1
#define MARKED_BITMAP 2
static ALLEGRO_BITMAP *bitmaps[3];

typedef struct {ALLEGRO_MUTEX *m; ALLEGRO_COND *c;} Barrier;

Barrier new_barrier() {
    Barrier b;
    b.m = al_create_mutex();
    b.c = al_create_cond();
    return b;
}

void free_barrier(Barrier bar) {
    al_destroy_cond(bar.c);
    al_destroy_mutex(bar.m);
}

static void free_bitmaps() {
    int i;
    for (i = 0; i < NUM_BITMAPS; ++i) {
        if (bitmaps[i] != NULL) {
            al_destroy_bitmap(bitmaps[i]);
            bitmaps[i] = NULL;
        }
    }
}

static void prepare_bitmaps(int w) {
    free_bitmaps();
    int i;
    for (i = 0; i < NUM_BITMAPS; ++i) {
        bitmaps[i] = al_create_bitmap(w, w);
        if(!bitmaps[i]) {
            fprintf(stderr, "failed to create bitmap!\n");
            exit(1);
        }
        al_set_target_bitmap(bitmaps[i]);
        al_clear_to_color(al_map_rgba(0, 0, 0, 0));
        switch (i) {
            case X_BITMAP:
                al_draw_line(w / 10.0, w / 10.0, w - (w / 10.0), w - (w / 10.0), al_map_rgb(0, 0, 0), w / 20.0);
                al_draw_line(w / 10.0, w - (w / 10.0), w - (w / 10.0), w / 10.0, al_map_rgb(0, 0, 0), w / 20.0);
                break;

            case O_BITMAP:
                al_draw_circle(w / 2.0, w / 2.0, w / 2.0 - (w / 10.0), al_map_rgb(0, 0, 0), w / 20.0);
                break;

            case MARKED_BITMAP:
                al_clear_to_color(al_map_rgb(200,200,250));
                break;
        }
    }
}

static void board_draw(ALLEGRO_DISPLAY *board_display, int PX) {
    al_set_target_backbuffer(board_display);
    al_clear_to_color(al_map_rgb(240,240,240));

    int x, y;
    for (x = 0; x < BOARD_SIZE; ++x) {
        for (y = 0; y < BOARD_SIZE; ++y) {
            if (board[x][y].marked) {
                al_draw_bitmap(bitmaps[MARKED_BITMAP], PX * x, PX * y, 0);
            }
            if (board[x][y].type != empty) {
                al_draw_bitmap(bitmaps[board[x][y].type == X ? X_BITMAP : O_BITMAP], PX * x, PX * y, 0);
            }
        }
    }

    ALLEGRO_COLOR board_line_color = al_map_rgb(170, 170, 170);
    float board_line_thickness = PX / 50.0;
    for (x = 1; x < BOARD_SIZE; ++x) {
        al_draw_line(PX * x, 0, PX * x, PX * BOARD_SIZE, board_line_color, board_line_thickness);
        al_draw_line(0, PX * x, PX * BOARD_SIZE, PX * x, board_line_color, board_line_thickness);
    }

    al_flip_display();
}

static void *board_runtime(ALLEGRO_THREAD *thread, void *initialization_barrier) {
    ALLEGRO_DISPLAY *board_display = NULL;
    ALLEGRO_EVENT_QUEUE *board_event_queue = NULL;

    if(!al_init()) {
        fprintf(stderr, "failed to initialize allegro!\n");
        exit(1);
    }

#if ALLEGRO_SUB_VERSION >= 1 || (ALLEGRO_SUB_VERSION >= 0 && ALLEGRO_WIP_VERSION >= 9)
    if (!al_init_native_dialog_addon()) {
        fprintf(stderr, "failed to initialize native dialog addon!\n");
        exit(1);
    }
#endif

    if (!al_init_primitives_addon()) {
        fprintf(stderr, "failed to initialize allegro primitives!\n");
        exit(1);
    }

    if (!al_install_mouse()) {
        fprintf(stderr, "failed to initialize the mouse!\n");
        exit(1);
    }

    al_set_new_display_flags(ALLEGRO_WINDOWED | ALLEGRO_RESIZABLE);

    int PX = 160;

    board_display = al_create_display(PX * BOARD_SIZE, PX * BOARD_SIZE);
    if(!board_display) {
        fprintf(stderr, "failed to create display!\n");
        exit(1);
    }

    board_event_queue = al_create_event_queue();
    if (!board_event_queue) {
        al_destroy_display(board_display);
        fprintf(stderr, "failed to create event_queue!\n");
        exit(1);
    }

    al_register_event_source(board_event_queue, al_get_display_event_source(board_display));
    al_register_event_source(board_event_queue, al_get_mouse_event_source());
    al_register_event_source(board_event_queue, &board_user_event_source);

    prepare_bitmaps(PX);
    board_draw(board_display, PX);

    {
        Barrier *b = (Barrier *) initialization_barrier;
        al_lock_mutex(b->m);
        al_broadcast_cond(b->c);
        al_unlock_mutex(b->m);
    }

    int redraw_board = 1;

    while (!al_get_thread_should_stop(thread)) {
        ALLEGRO_EVENT ev;
        if(al_wait_for_event_timed(board_event_queue, &ev, 0.5)) {
            BoardPos move;

            switch (ev.type) {
                case ALLEGRO_EVENT_DISPLAY_CLOSE:
                    move.x = move.y = -1;
                    push_move(move);
                    al_set_thread_should_stop(thread);
                    break;

                case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
                    move.x = ev.mouse.x / PX;
                    move.y = ev.mouse.y / PX;
                    if (move.x >= 0 && move.x < BOARD_SIZE && move.y >= 0 && move.y < BOARD_SIZE) {
                        push_move(move);
                    }
                    break;

                case ALLEGRO_EVENT_DISPLAY_RESIZE: {
                    int old_PX = PX;
                    PX = (ev.display.width < ev.display.height ? ev.display.width : ev.display.height) / BOARD_SIZE;
                    if (old_PX != PX) prepare_bitmaps(PX);
                    al_acknowledge_resize(board_display);
                }

                case BOARD_USER_EVENT_REDRAW:
                    redraw_board = 1;
                    break;

                case BOARD_USER_EVENT_STOP:
                    al_set_thread_should_stop(thread);
                    break;

                case BOARD_USER_EVENT_MESSAGE_BOX: {
                    Barrier *b = (Barrier *) ev.user.data2;
                    char *str = (char *) ev.user.data1;

                    al_lock_mutex(b->m);
                    al_show_native_message_box(board_display, "Info", "Info", str, NULL, 0);
                    al_broadcast_cond(b->c);
                    al_unlock_mutex(b->m);

                    al_flush_event_queue(board_event_queue);
                    break;
                }
            }

            if (ALLEGRO_EVENT_TYPE_IS_USER(ev.type)) {
                al_unref_user_event(&ev.user);
            }

            al_lock_mutex(board_mutex);
            if (redraw_board) {
                board_draw(board_display, PX);
                redraw_board = 0;
            }
            al_unlock_mutex(board_mutex);
        }
    }

    al_destroy_display(board_display);
    al_destroy_event_queue(board_event_queue);

    return NULL;
}

void board_redraw() {
    ALLEGRO_EVENT ev;
    ev.user.type = BOARD_USER_EVENT_REDRAW;
    al_emit_user_event(&board_user_event_source, &ev, NULL);
}

void board_init() {
    board_mutex = al_create_mutex();
    moves_mutex = al_create_mutex();
    moves_cond = al_create_cond();
    al_init_user_event_source(&board_user_event_source);

    Barrier b = new_barrier();
    al_lock_mutex(b.m);

    board_thread = al_create_thread(board_runtime, &b);
    if (!board_thread){
        fprintf(stderr, "failed to create thread!\n");
        exit(-1);
    }

    al_start_thread(board_thread);
    al_wait_cond(b.c, b.m);
    al_unlock_mutex(b.m);
    free_barrier(b);
}

void board_free() {
    ALLEGRO_EVENT ev;
    ev.user.type = BOARD_USER_EVENT_STOP;
    al_emit_user_event(&board_user_event_source, &ev, NULL);
    al_join_thread(board_thread, NULL);
    al_destroy_user_event_source(&board_user_event_source);
    al_destroy_thread(board_thread);
    al_destroy_mutex(board_mutex);
    al_destroy_mutex(moves_mutex);
    al_destroy_cond(moves_cond);
}

void board_place(BoardPos pos, BoardFieldType t) {
    al_lock_mutex(board_mutex);
    board[pos.x][pos.y].type = t;
    board_redraw();
    al_unlock_mutex(board_mutex);
}

void board_mark(BoardPos pos) {
    al_lock_mutex(board_mutex);
    board[pos.x][pos.y].marked = 1;
    board_redraw();
    al_unlock_mutex(board_mutex);
}

void board_unmark(BoardPos pos) {
    al_lock_mutex(board_mutex);
    board[pos.x][pos.y].marked = 0;
    board_redraw();
    al_unlock_mutex(board_mutex);
}

BoardPos board_get_move() {
    return pop_move();
}

void board_message(const char *format, ...) {
    char *buffer = malloc(256);
    va_list args;
    va_start(args, format);
    vsnprintf(buffer, 256, format, args);
    va_end(args);

    Barrier b = new_barrier();
    al_lock_mutex(b.m);
    ALLEGRO_EVENT ev;
    ev.user.type = BOARD_USER_EVENT_MESSAGE_BOX;
    ev.user.data1 = (intptr_t) buffer;
    ev.user.data2 = (intptr_t) &b;
    al_emit_user_event(&board_user_event_source, &ev, NULL);
    al_wait_cond(b.c, b.m);;
    al_unlock_mutex(b.m);
    free_barrier(b);
    free(buffer);
}
